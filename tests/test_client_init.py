import os
import pytest

import pyveris.constants as const
from pyveris.client import Client


class TestInit:

    @staticmethod
    def test_client_init_error_api_config_not_found():
        with pytest.raises(KeyError):
            Client()

    @staticmethod
    def test_client_init_error_api_config_not_json():
        os.environ[const.api_config_name] = "not json"
        with pytest.raises(ValueError) as exc_info:
            Client()
        assert (
            exc_info.value.args[0]
            == f"Environment variable {const.api_config_name} should be set to a JSON string"
        )

    @staticmethod
    def test_client_init_error_incomplete_api_config():
        os.environ[const.api_config_name] = '{"username": "tester"}'
        with pytest.raises(KeyError) as exc_info:
            Client()
        assert (
            exc_info.value.args[0]
            == f"{const.api_config_url_name} must be present in {const.api_config_name}; not found"
        )

    @staticmethod
    def test_client_init_ok():
        import local.dev_config as conf

        c = Client()
        assert c.base_url == conf.converis_api_config[const.api_config_url_name]
