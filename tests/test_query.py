import json
import pytest
from datetime import datetime
from http import HTTPStatus

from pyveris.client import Client


class TestQuery:
    @classmethod
    def setup_class(cls):
        import local.dev_config as conf
        cls.client = Client()
        cls.default_query = f"[Updated on] >= {datetime.today().strftime('%Y-%m-%d')}"

    def test_min_query_ok(self):
        r = self.client.query_data_entities(query=self.default_query)
        assert HTTPStatus.OK == r.status_code

    def test_query_missing_error(self):
        with pytest.raises(KeyError) as exc_info:
            self.client.query_data_entities()
        assert (
                exc_info.value.args[0]
                == "You must include a 'query' parameter when calling this method"
        )

    def test_query_optional_params(self):
        r = self.client.query_data_entities(
            query=self.default_query,
            attribute_definition="BASIC",
            count=5,
            language="en_GB",
            sort="Created on",
            sortOrder="DESC",
            startRecord=2
        )
        assert HTTPStatus.OK == r.status_code
        assert 5 == json.loads(r.text)["count"]
