# pyveris

[Converis](https://clarivate.com/webofsciencegroup/solutions/converis/) API client written in python.

## Installation

```
$ pip install pyveris
```

## Usage

```
import json
import os
from pprint import pprint
from pyveris.client import Client

api_config = {
    "url": "https://api.clarivate.com/converisreadapi/"  # url to root of API of your Converis instance
    "username": "your_username"
    "password": "your_password"
}

os.environ["CONVERIS_API_CONFIG"] = json.dumps(api_config)

c = Client()
r = c.query_data_entities(query="[Updated on] >= 2022-11-15")
pprint(json.loads(r.text))
```